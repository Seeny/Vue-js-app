// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import vueResource from 'vue-resource';
import vueRouter from 'vue-router';
import test from './components/test';
import user from './components/user';

vueResource.use(vueRouter);
Vue.use(vueResource);


const router = new vueRouter({
  mode:'history',
  base: __dirname,
  routes: [
    {
      path:'/',
      component: user
    },
    {
      path:'/test',
      component: test
    }
  ]

})

Vue.config.productionTip = false
/* eslint-disable no-new */
new Vue({
  router,
  components: { App },
  template: '<App/>'
}).$mount('#app')
